/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesis.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import PlaceRecognition.PlaceRecognition;
import com.mathworks.toolbox.javabuilder.MWException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Decoder;

/**
 *
 * @author ptrwoz
 */
@Controller
public class mainController {

    @Autowired(required = true)
    PlaceRecognition placeRecognition;
    @Autowired(required = true)
    @Qualifier("placeRecognitionParams")
    Object[] placeRecognitionParams;

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    @ResponseBody
    String getResult(@RequestParam String imagePath) throws MWException {
        Object[] objArray = (Object[]) placeRecognitionParams[0];
        Object[] result = placeRecognition.predict_VGG(1, objArray[0], objArray[1], objArray[2], imagePath);
        return result[0].toString();
    }
    @RequestMapping(value = "/image", method = RequestMethod.POST, headers="Accept=*/*",  produces="application/json")
    @ResponseBody String imageResult(@RequestBody String textImage) throws MWException, IOException {

        BufferedImage image = this.decodeToImage(textImage);
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss") ;
        File outputfile = new File("images/" + dateFormat.format(date) + ".png");
        ImageIO.write(image, "jpg", outputfile);
        Object[] objArray = (Object[]) placeRecognitionParams[0];
        Object[] result = placeRecognition.predict_VGG(2, objArray[0], objArray[1], objArray[2], outputfile.getCanonicalPath());
        return result[0].toString();
    }
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    String test() throws MWException {
        return "test";
    }
    private BufferedImage decodeToImage(String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
}
