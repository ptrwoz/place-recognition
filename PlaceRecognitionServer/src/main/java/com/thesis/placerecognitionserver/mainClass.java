package com.thesis.placerecognitionserver;

import PlaceRecognition.PlaceRecognition;
import com.mathworks.toolbox.javabuilder.MWException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;

@EnableAutoConfiguration
@Configuration
@ComponentScan(value = "com.thesis.controller")
@RestController
public class mainClass {

    public PlaceRecognition placeRecognition;
    public Object[] placeRecognitionParams;
    public String testText;
    public static void main(String[] args) throws Exception {
        SpringApplication.run(mainClass.class, args);
    }

    @Bean(name = "placeRecognition")
    public PlaceRecognition placeRecognition() throws MWException {
        placeRecognition = new PlaceRecognition();
        return placeRecognition;
    }

    @Bean(name = "placeRecognitionParams")
    public Object[] placeRecognitionParams() throws MWException {
        placeRecognitionParams = this.placeRecognition.load_VGG(3);
        return placeRecognitionParams;
    }
}
