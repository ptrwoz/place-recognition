function [result, maxScore] = predict_VGG(convnet,featureLayer,classifier,imagePath)
    img = imread(imagePath);
    img = imresize(img,[224 224]);
    features1 = activations(convnet, img, featureLayer, 'MiniBatchSize', 32, 'OutputAs', 'columns');
    [predictedLabels, score] = predict(classifier, features1.');
    result = char(predictedLabels);
    %maxScore = max(score)
    maxScore = score * -1;
    maxScore = maxScore * 100;
    maxScore = 100 - maxScore; 
    result
end
