%% CrossValidation function 
% Take imageDatastore, features and n-crossVal param
function allDataParts = cross_validation(imds, features, k)
    sumAll = 0;
    allDataParts = [];
    tb = countEachLabel(imds);
    for i=1:height(tb)
        index = randperm(tb(i,2).Count,tb(i,2).Count);
        imds1 = [];
        for j=1:tb(i,2).Count
            imds1.Features(j,:) =  features(:,sumAll+index(j));
            imds1.Labels(j,:) = imds.Labels(sumAll+index(j),:);
            imds1.Files(j,:) = imds.Files(sumAll+index(j),:);
        end
        parts = 1:(int16(tb(i,2).Count/k)):tb(i,2).Count;
        dataParts = {};
        for j=1:k 
           if(j+1>k) 
                dataParts{j}.features = imds1.Features(parts(j):tb(i,2).Count,:);
                dataParts{j}.labels = imds1.Labels(parts(j):tb(i,2).Count,:);
                dataParts{j}.files = imds1.Files(parts(j):tb(i,2).Count,:);
           else
                dataParts{j}.features= imds1.Features(parts(j):parts(j+1)-1,:);
                dataParts{j}.labels = imds1.Labels(parts(j):parts(j+1)-1,:);
                dataParts{j}.files = imds1.Files(parts(j):parts(j+1)-1,:);
           end           
        end
        allDataParts = [allDataParts; dataParts];
        sumAll = sumAll + tb(i,2).Count;
    end 
end