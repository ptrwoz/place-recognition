%% Div data on blur/noblur function 
% Take imageDatastore, param blur or not
function newData = divData(imds,blur)
    file = imds.Files;
    label = imds.Labels;
    features = imds.Features;
    %featuresRot = imds.FeaturesRot;
    nlabel = [];
    nfile = [];
    nfeatures = [];
    %nfeaturesRot = [];
    if blur==string('blur')
        for i = 1:length(file)
            image = file{i,1};
            array = strsplit(image,'_');
            if(array{1,4}=='1')
                 nlabel = [nlabel; label(i,:)];
                 nfile = [nfile; file(i,:)];
                 nfeatures = [nfeatures; features(i,:)];
                 %nfeaturesRot = [nfeaturesRot; featuresRot(i,:)];
            end
        end        
    else
        for i = 1:length(file)
            image = file{i,1};
            array = strsplit(image,'_');
            if(array{1,4}=='0')
                nlabel = [nlabel; label(i,:)];
                nfile = [nfile; file(i,:)];
                nfeatures = [nfeatures; features(i,:)];
                %nfeaturesRot = [nfeaturesRot; featuresRot(i,:)];
            end
        end 
    end
    newData.Files = nfile;
    newData.Features = nfeatures;
    newData.Labels = nlabel;
    %newData.FeaturesRot = nfeaturesRot;
end