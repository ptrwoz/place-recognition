%% Get data part after crossVal. 
% Take data from crossVal function and getting data indexs
function [train , test] = get_cross_validation_data(data,trainIndex)
    s = size(data);
    train.Features = [];
    train.Labels = [];
    train.Files = [];
    %train.FeaturesRot = [];
    test.Features = [];
    test.Labels = [];
    test.Files = []; 
    %test.FeaturesRot = [];
    for i=1:s(2)
        if(sum(ismember(trainIndex,i)))
            for j=1:s(1)
                %test.FeaturesRot = [test.FeaturesRot; data{j,i}.featuresRot];
                test.Features = [test.Features; data{j,i}.features];
                test.Labels = [test.Labels; data{j,i}.labels];
                test.Files = [test.Files; data{j,i}.files];
            end
        else
            for j=1:s(1)
                %train.FeaturesRot = [train.FeaturesRot; data{j,i}.featuresRot];
                train.Features = [train.Features; data{j,i}.features];
                train.Labels = [train.Labels; data{j,i}.labels];
                train.Files = [train.Files; data{j,i}.files];
            end
        end
    end
end