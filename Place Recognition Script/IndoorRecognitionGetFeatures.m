%% Transfer Deep Learning for Indoor Place Categorization for Robot
clear all;
clc;
%% Set Path for Pre-Trained CNN model
% Location of pre-trained "VGG-F"
% It works on 'imagenet-vgg-f.mat';
global imgNetSize;
cnnFolder = '../../../../imagenet-caffe-alex';
cnnMatFile = 'imagenet-vgg-f.mat';
% Location of result folder
ImageSequencesFolder = 'ImageSequences';
resultFolder = './results';
% Input image size
if strcmp(cnnMatFile, 'imagenet-vgg-f.mat') 
   imgNetSize = [224 224];
else
   imgNetSize = [227 227];     
end
cnnFullMatFile = fullfile(cnnFolder, cnnMatFile);
%% Room categories
% Room categoris in main folder
categories = {'balcony-corridor','balcony-elevator','conference-room',...
'elevator-room','exit-corridor','floor-corridor','hall-corridor',...
'hall-elevator','kitchen','kitchen-corridor','lab452','lab453',...
'lab456','lab453-corridor','lecture-room','student-room'};
%% Load Pre-trained CNN
% Load MatConvNet network into a SeriesNetwork
convnet = helperImportMatConvNet(cnnFullMatFile);
%% |convnet.Layers| defines the architecture of the CNN 
convnet.Layers
% Inspect the last layer
convnet.Layers(end)
% Number of class names for ImageNet classification task
numel(convnet.Layers(end).ClassNames)
%% Transfer Learning using VGG-F
% Note that the CNN model is not going to be used for the original
% classification task. It is going to be re-purposed to solve a different
% classification task on the pets dataset.
%% Extract training features using pretrained CNN
featureLayer = 'fc6'; % You can change to fc7 or conv3
% featureLayer = 'fc7';
%% Train a multiclass SVM classifier
% Load and prepare data
% Path to data folder 
DataFolder = 'DataPath';
SaveDataFile = 'DatasetRoom.mat';
%Load data from folder
imds = imageDatastore(fullfile(DataFolder, categories), 'LabelSource', 'foldernames');
% Get training labels from the trainingSet
imdsLabels = imds.Labels;
% Get the known labels
imds.ReadFcn = @(filename)readAndPreprocessImage(filename);
% Get images tabel
tbl = countEachLabel(imds);
% Get DataSet features
features = activations(convnet, imds, featureLayer, 'MiniBatchSize', 32, 'OutputAs', 'columns');
% Save data
save(SaveDataFile);
