%% Transfer Deep Learning for Indoor Place Categorization for Robot
%clear all;
%clc;
fprintf('Begin script...\n');
% Load data - result from IndoorRecognitionGetFeatures
%load('DatasetNew.mat');
% Save result
resultFilePath = 'expResult.txt';
resultFile = fopen(resultFilePath,'a');
% Start experiment
time = datetime('now');
fprintf(resultFile,'%s\n',time);
%% Cross Validation function take imageDatastore features and n-cross param 
nParam = 10;
testSize = 3;
%path = 'D:\nauka\doktorat\praca-doktorancka\DataSet\DataSetNao\undefined\Original_1.png';
fprintf('Begin cross validation...\n');
%[imds, features] = undefinedDataGenerator(imds, features,path , 600);
dataParts = cross_validation(imds, features, nParam);

fprintf(resultFile,'CrossValParts %d TestSize %d\n',nParam,testSize);
% Sector getting to experiments 
sectors = {[1,2,3],[2,3,4],[3,4,5],...
    [4,5,6],[5,6,7],[6,7,8],...
    [7,8,9],[8,9,10],[9,10,1],...
    [10,1,2]};
%% Room categories
% Room categoris in main folder
% categories = {'balcony-corridor','balcony-elevator','conference-room',...
% 'elevator-room','exit-corridor','floor-corridor','hall-corridor',...
% 'hall-elevator','kitchen','kitchen-corridor','lab452','lab453',...
% 'lab456','lab453-corridor','lecture-room','student-room'};
categories = {'sector1','sector2','sector3'};
fprintf('Begin SVM test...\n');
%% Main each 
for g = 1:nParam
    % Get test and train data after crossVal.
    [train, test] = get_cross_validation_data(dataParts,sectors{1,g});
    %% Reduce data blur/noblur
    %train = get_divided_data(train,'noBlur');
    %test = get_divided_data(test,'blur');
    %test.Features = test.FeaturesRot;
    % Get train datamin1
    trainFeatures = train.Features;
    trainLabels = train.Labels; 
    %% Train multiclass SVM classifier using a fast linear solver, and set
    % 'ObservationsIn' to 'columns' to match the arrangement used for training features.
    classifier = fitcecoc(trainFeatures.', trainLabels, ...
        'Learners', 'Linear', 'Coding', 'onevsall', 'ObservationsIn', 'columns');
    %%import matlab variable testData
    % Get test data
    testFeatures = test.Features; 
    testLabels = test.Labels;
    imds.Files = test.Files;
    imds.Labels = test.Labels;  
    tbl_test = countEachLabel(imds);
    %% Predict test DataSet
    [predictedLabels, score] = predict(classifier, testFeatures);
    %% Get metod result
    % Get train data statistic
    num_in_class = table2array(tbl_test(:,2));
    name_class = tbl_test(:,1);
    % Change actual label on number
    actual_label = [];   
    for a = 1:length(predictedLabels)
        for b = 1:size(name_class)
            if testLabels(a)==name_class.Label(b)
                actual_label = [actual_label; b];
            end
        end
    end
    % Change predict label on number
    predict_label = [];
    for a = 1:length(predictedLabels)
        for b = 1:size(name_class)
            if predictedLabels(a)==name_class.Label(b)
                predict_label = [predict_label; b];
            end
        end
    end
    decision_values = score;
    tmp = [decision_values predict_label];
    tmp(1:5, :);
    %% Get result properties
    label_or_decision='decision'; % use label('label') as decision or decision_values('decision') as decision decision will be better.
    compute_precision_recall(predict_label,decision_values,actual_label,num_in_class,label_or_decision);
    %% compute accuracy and F-measure, etc.
    classes = [1:max(max(actual_label),max(predict_label))];
    %fprintf('Begin computing confus,accuracy,numcorrect,precision,recall,F...\n');
    [confus,accuracy,numcorrect,precision,recall,F,PatN,MAP,NDCGatN]=compute_accuracy_F(actual_label,predict_label,classes);    
    fprintf('[Result] %d accuracy: %.4f precision: %.4f recall: %.4f F: %.4f PatN: %.4f numcorrect: %d MAP: %.4f \n',g,accuracy,mean(precision),mean(recall),mean(F), mean(PatN), numcorrect, MAP);
    fprintf(resultFile,'%d %.4f %.4f %.4f %.4f %.4f %d %f\n',g,accuracy,mean(precision),mean(recall),mean(F), mean(PatN), numcorrect, MAP);
end
%close file with experiment results 
fclose(resultFile);

%% View last result
% Init video param
fprintf('Begin video...\n');
ImageSequencesFolder = 'ImageSequences';
testVideo = VideoWriter( fullfile( ImageSequencesFolder, 'PlaceRec_Dataset'));
testVideo.FrameRate = 60;
open(testVideo);
numTestImgs = length(test.Files);
true_color = {'green'};
false_color = {'red'};
incorrect = 0;
correct = 0;
% View main each
for ii=1:numTestImgs,
    fName = test.Files{ii};
    im = imread(fName);
    position = [400 10;]; 
    im = insertText(im,[0 10],strcat('correct_',num2str(correct)));
    im = insertText(im,[100 10],strcat('incorrect_',num2str(incorrect)));
    im = insertText(im,[200 10],categories{1,actual_label(ii,1)});
    a = categories{1,actual_label(ii,1)};
    p = categories{1,predict_label(ii,1)};
    % check clashyfication result
    if(string(a)==string(p))
        im = insertText(im,position,categories{1,predict_label(ii,1)},'BoxColor',true_color);
        correct = correct + 1;
    else
        im = insertText(im,position,categories{1,predict_label(ii,1)},'BoxColor',false_color);
        incorrect = incorrect + 1;
    end
    % show image 
    %imshow(im);
    %drawnow;
    ii;
    for k = 1:3 
        writeVideo( testVideo, im);  
    end
    clear im;
    %pause(0.1);
    %pause
end
% close video file
close(testVideo);
fprintf('End script...\n');


