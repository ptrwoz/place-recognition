function [features] = getFeatures(convnet,featureLayer,classifier,imagePath)
    img = imread(imagePath);
    img = imresize(img,[224 224]);
    features = activations(convnet, img, featureLayer, 'MiniBatchSize', 32, 'OutputAs', 'columns');
end